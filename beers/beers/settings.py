# -*- coding: utf-8 -*-

# Scrapy settings for beers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'beers'

SPIDER_MODULES = ['beers.spiders']
NEWSPIDER_MODULE = 'beers.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'beers (+http://www.yourdomain.com)'
